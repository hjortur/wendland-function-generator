#ifndef SDECOLLOC_H
#define SDECOLLOC_H
#include "point.h"

double *rhs_v(struct point *x, int sizeX,struct point *s, int sizeS);

double Azero_v(struct point XL, struct point XK);

double Ay_v(struct point XL, struct point XK);

double Axy_v(struct point XL, struct point XK);

double *collocmatrix(struct point *Y, int sizeY, struct point *S,int sizeS);

double *collocmatrixpacked(struct point *Y, int sizeY, struct point *S, int sizeS);

double *evalfunclv(struct point *X, int sizeX, struct point *Y, int sizeY, struct point *S,int sizeS,double *alpha);
double *evalfunc(struct point *X, int sizeX, struct point *Y, int sizeY, struct point *S,int sizeS,double *alpha);
#endif
