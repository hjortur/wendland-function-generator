#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cblas.h>
#include <lapacke.h>
#include "wendlandfunctions.h"
#include "point.h"
#include "ddecolloc.h"

#define NX 20
#define NY 20

struct point zero;

struct point f(struct point x)
{
    struct point ret;
    ret.x[0]=x.x[1];
    ret.x[1]=-x.x[0]-(1.0-x.x[0]*x.x[0])*x.x[1];
    
    return ret;
}

double rhslv(struct point x)
{
    return -normp(x);
}


int main(int argc, char **argv)
{
    int sizeY=0,mklone=1, info;
    struct point a={-2.0,-2.0};
    struct point b={2.0,2.0};
    struct point Ny={NY,NY};

    //struct point *Y=generategrid(a,b,Ny, 0.1, 8.0,&sizeY);
    struct point *Y=generateoptimalgrid(a,b,4.0/15.0,0.1,8.0 ,&sizeY);
    double *A=collocmatrix(Y, sizeY);
    double *r=rhs_v(Y, sizeY);

    info=LAPACKE_dposv(LAPACK_ROW_MAJOR,'U', sizeY, 1,A, sizeY, r, 1);
    if( info>0)
    {
        printf("the leading minor of order %lld of Collocmatrix is not positive definite.\nSolution could not be computed\n", info);
    }

    struct point Nx={NX,NX};
    int sizeX;
    struct point *X=generategrid(a,b,Nx,-1.0,10.0, &sizeX);
    double *xvalues=evalfunc(X,sizeX, Y, sizeY, r);
    
    printresults(X,sizeX, xvalues);

    free(xvalues);
    free(X);
    free(r);
    free(A);
    free(Y);
    return 0;
}
