#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <cblas.h>
#include <lapacke.h>

#include "wendlandfunctions.h"
#include "point.h"
#include "sdecolloc.h"

#define NX 200
#define NY 400 

#define SMALL_R 0.1
#define BIG_R 8.0



struct point f(struct point x)
{
    struct point ret;
    ret.x[0]=sin(x.x[0]);
    
    return ret;
}

int dimg(void)
{
	return 1;
}

struct point *g(struct point X,int *size)
{
	int Q=dimg(); //dimension of brownian motion should be less then dim
	struct point *ret=malloc(Q*sizeof(*ret));
	ret->x[0] = 3.0*X.x[0]/(1.0+X.x[0]*X.x[0]);
	*size=Q;
	return ret;
}


double rhsv(struct point x)
{
	double ret=(normp(x)-SMALL_R)/(BIG_R-SMALL_R);
	return ret;
}

double rhslv(struct point x)
{
    return -0.0001;
}

int main(int argc, char **argv)
{
    int sizeY=0,sizeS=2, info;
    struct point a={SMALL_R};
    struct point b={8.0};
    struct point Ny={NY};
    struct point pBR={BIG_R};
    
    //struct point *Y=generategrid(a,b,Ny,SMALL_R,2*BIG_R,&sizeY);
    struct point *Y=generateoptimalgrid(a,b,1.0/NY, SMALL_R, 10.0, &sizeY);
    //printf("generated colloc grid size of grid %d\nNow boundary",sizeY);
    //printpoints(Y,sizeY, "Y");
    struct point Ns={2.0};
    struct point *S=generategrid(a, pBR,Ns, -1.0, 10.0, &sizeS);
    double *A=collocmatrix(Y,sizeY,S, sizeS);
    double *r=rhs_v(Y,sizeY, S,sizeS);
    info=LAPACKE_dposv(LAPACK_ROW_MAJOR,'U', sizeY+sizeS, 1,A, sizeY+sizeS, r, 1);
    if( info>0)
    {
        printf("the leading minor of order %lld of Collocmatrix is not positive definite.\nSolution could not be computed\n", info);
    }
    struct point zero={0.0};
    struct point Nx={NX};
    int sizeX;
    struct point *X=generategrid(zero,b, Nx, -1.0, 10.0,&sizeX );
    double *xvalues=evalfunc(X,sizeX, Y, sizeY,S,sizeS, r);
	
    printresults(X,sizeX, xvalues);    

    free(xvalues);
    free(X);
    free(r);
    free(S);
    free(A);
    free(Y);
    return 0;
}
