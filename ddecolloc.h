#ifndef DDECOLLOC_H
#define DDECOLLOC_H
#include "point.h"

double *rhs_v(struct point *x, int size);	

double Ay(struct point Y, struct point X);
double Axy(struct point Y, struct point X);

double *collocmatrix(struct point *Y, int sizeY);

double *evalfunc(struct point *X, int sizeX, struct point *Y, int sizeY, double *alpha);
#endif
