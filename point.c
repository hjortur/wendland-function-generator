#include <math.h>
#include <assert.h>
#include <malloc.h>
#include <stdio.h>
#include "point.h"
struct point constpoint(double a)
{
    struct point t;
    for(int i=0;i<DIM;i++)
    {
        t.x[i]=a;
    }
    return t;
}

double delta(int i, int j)
{
	if(i==j)
		return 1.0;
	else
		return 0.0;
}
double proj(struct point x, int j)
{
	assert(j<DIM);
	return x.x[j];
}
struct point *generate2dboundarygrid(double r, double R, int sizeinner, int sizeouter)
{
	struct point *ret=malloc((sizeinner+sizeouter)*sizeof(struct point));
	for(int i=0;i<sizeinner;i++)
	{
		ret[i].x[0]=r*cos(((i*2.0)/sizeinner)*M_PI);
		ret[i].x[1]=r*sin(((i*2.0)/sizeinner)*M_PI);
	}
	for(int j=0;j<sizeouter;j++)
	{
		ret[j+sizeinner].x[0] =R*cos(((j*2.0)/sizeouter)*M_PI);
		ret[j+sizeinner].x[1]=R*sin(((j*2.0)/sizeouter)*M_PI);
	}
	return ret;
}

struct point *generategrid(struct point a, struct point b, struct point N, double r, double R, int *size)
{
    int BOUND=1,count=0;
    struct point delta=addp(b,a,1.0,-1.0);
    for(int i=0;i<DIM;i++)
    {
        BOUND=BOUND*(int)N.x[i];
        delta.x[i]=delta.x[i]/((int)N.x[i]-1);
    }
    struct point *res=malloc(BOUND*sizeof(*res));
    for(int i=0;i<BOUND;i++)
    {
        int curr=i,temp;
        struct point t;
        for(int j=0;j<DIM;j++)
        {
            temp=curr%((int)N.x[j]);
            curr=curr/((int)N.x[j]);
            t.x[j]=a.x[j]+delta.x[j]*temp;
        }
        double norm=normp(t);
        if(norm>r && norm <= R)
        {
            res[count++]=t;
        }
    }
    res=realloc(res,count*sizeof(struct point));
    *size=count;
    return res;
}

void pushback(struct point x, struct _grid *M)
{
	assert(M->size>=M->current);
	if(M->size == M->current)
	{
		M->grid=realloc(M->grid, 2*M->size*sizeof(struct point));
		M->size=2*M->size;	
	}
	M->grid[M->current++]=x;
	return;
}

void ML(int r, struct point x,struct point a,struct point b, double *e, double alpha,struct _grid *M, struct point *W, double inner, double outer)
{
	int start=(int)(ceil((a.x[r]-x.x[r])/(alpha*(r+2)*e[r])));
	int end=(int)(floor((b.x[r]-x.x[r])/(alpha*(r+2)*e[r])));
	for(int i=start;i<=end; i++)
	{
		if(r==0)
		{
			struct point temp=addp(x,W[r],1.0,i*alpha);
			if(normp(temp)>=inner && normp(temp)<=outer)
				pushback(temp,M);
		}
		else{
			ML(r-1, addp(x,W[r], 1.0, i*alpha), a,b,e,alpha,M,W,inner, outer);
		}
	}
	return;
}

int optimalgridhelper(struct point a, struct point b, double alpha, int dimension, struct _grid *M, double inner, double outer)
{	
	int i,k;
	double e[dimension];
	for(k=1;k<=dimension;k++)
	{
		e[k-1]=sqrt(1.0/(2.0*k*(k+1)));
	}
	struct point W[dimension];
	struct point v=constpoint(0.0);
	for(i=1;i<=dimension;i++)
	{
		v.x[i-1]=(i+1.0)*e[i-1];
		W[i-1]=v;
		v.x[i-1]=e[i-1];
	}
	ML(dimension-1,mulp(W[dimension-1],0.5*alpha),a,b, e, alpha,M,&W[0],inner, outer);
	return M->current;
}
struct point *generateoptimalgrid(struct point a, struct point b, double alpha, double inner, double outer, int *sizegrid)
{
	struct _grid M;
	struct point tol=constpoint(1e-10);
	M.size=1;
	M.grid=malloc(M.size*sizeof(struct point));
	M.current=0;
	a=addp(a,tol,1.0,-1.0);
	b=addp(b,tol,1.0,1.0);
	M.current=optimalgridhelper(a,b,alpha,DIM,&M,inner,outer);
	*sizegrid=M.current;
	return M.grid;
}

double *symmetrize(double *M, int size)
{
    double *ret =malloc(size*size*sizeof(*ret));
    for(int n=0;n<size;n++)
    {
        for(int j=n;j<size;j++)
        {
            double temp=(M[size*n+j] + M[size*j+n])/2.0;
            ret[size*n+j]=temp;
            ret[size*j+n]=temp;
        }
    }
    return ret;
}


double *outer(struct point x, struct point y)
{
    double *ret=malloc(DIM*DIM*sizeof(*ret));
    for(int i=0;i<DIM;i++)
    {
        for(int j=0;j<DIM;j++)
        {
            ret[i*DIM+j]=x.x[i]*y.x[j];
        }
    }
    return ret;
}

double *eye(int N)
{
    double *ret=malloc(N*N*sizeof(*ret));
    for(int i=0;i<N;i++)
    {
        for(int j=0;j<N;j++)
        {
            ret[i*N+j]=0.0;
            if(i==j)
                ret[i*N+j]=1.0;
        }
    }
    return ret;
}


double *matrixadd(double * A, double* B, int m, int n, double alpha, double beta)
{
    double *res=malloc((m*n)*sizeof(*res));
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            res[i*n+j]=alpha*A[i*n+j]+beta*B[i*n+j];
        }
    }
    return res;
}

double *matrixmul(double *A, int rowa, int cola, double *B, int rowb, int colb)
{
    assert(cola==rowb);
    double temp;
    double *res=malloc((rowa*colb)*sizeof(*res));
    for(int i=0;i<rowa;i++)
    {
        for(int j=0;j<colb;j++)
        {
            temp=0.0;
            for(int k=0;k<cola;k++)
            {
                temp=temp+A[cola*i+k]*B[colb*k+j];
            }
            res[colb*i+j]=temp;
        }
    }
    return res;
}

double trace(double *M, int n)
{
    double res=0.0;
    for(int i=0;i<n;i++)
    {
        res=res+M[i*n+i];
    }
    return res;
}

double innerp(struct point x, struct point y)
{
    double acc=0.0;
    for(int i=0;i<DIM;i++)
    {
        acc+=x.x[i]*y.x[i];        
    }
    return acc;
}

double normp(struct point x)
{
    double acc=0.0;
    for(int i=0;i<DIM;i++)
    {
        acc+=x.x[i]*x.x[i];
    }
    return sqrt(acc);
}
struct point mulp(struct point x, double a)
{
    struct point res;
    for(int i=0;i<DIM;i++)
    {
        res.x[i]=x.x[i]*a;
    }
    return res;
}

struct point addp(struct point x, struct point y, double a, double b)
{
    struct point res;
    for(int i=0;i<DIM;i++)
    {
        res.x[i]=x.x[i]*a + y.x[i]*b;
    }
    return res;
}

void printmatrix(double *A, int m, int n, const char *s)
{
	printf("Matrix %s is (%d,%d)\n",s,m,n); 
	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++)
		{
			printf("%f ",A[i*n+j]);
		}
		printf("\n");
	}
	printf("End matrix %s\n",s);
	return;
}

void printpoints(struct point *X, int sizeX, const char *s)
{
	printf("Points %s:(%d)\n",s,sizeX);
	for(int i=0;i<sizeX;i++)
	{
		for(int d=0;d<DIM;d++)
		{
			printf("%f ",X[i].x[d]);
		}
		printf("\n");
	}
	printf("End points %s\n",s);
	return;
}

void printresults(struct point *X, int sizeX, double *alpha)
{
	for(int i=0;i<sizeX;i++)
	{
		for(int j=0;j<DIM;j++)
		{
			printf("%f ",X[i].x[j]);
		}
		printf("%f\n", alpha[i]);
	}
	return;
}

double matrixnorm(double *M, int rows, int cols)
{
	double temp=0.0,max=0.0;
	for(int i=0;i<rows;i++)
	{
		temp=0;
		for(int j=0;j<cols;j++)
		{
			temp = temp+fabs(M[i*cols+j]);
		}
		if(temp>max)
			max=temp;
	}
	return max;	
}

double matrixnormpacked(double *M, int n)
{
	int t=0,t2;
	double temp=0.0,max=0.0;
	for(int i=0;i<n;i++)
	{
		temp=0.0;
		t2=0;
		for(int k=0;k<i;k++)
		{
			assert((t2+i-k)< n*(n+1)/2); 
			temp=temp+fabs(M[t2+i-k]);
			t2=t2+(n-k);
		}
		for(int k=i;k<n;k++)
		{
			assert((t+(k-i))< n*(n+1)/2);
			temp=temp+fabs(M[t+(k-i)]);
		}
		t=t+(n-i);
		if(temp>max)
		{
		  max=temp;
		}
	}
	return max;
}


