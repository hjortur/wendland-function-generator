/* *****************************************************************
    This file is automatically generated, it includes definitions or
    declerations for various wendland functions and families used as RBF 
    functions for collocation problems
***************************************************************** */
// file:wendlandfunctions.h
#ifndef WENDLANDFUNCTIONS_H_
#define WENDLANDFUNCTIONS_H_
#ifdef __cplusplus
extern "C" {
#endif

#define _join2(x, y,z) __wendlandpsi_##x##_##y##_##z
#define _join(x,y,z) _join2(x,y,z)
#define PSI(N) _join(WL,WK, N)

#define PSI0 PSI(0)
#define PSI1 PSI(1) 
#define PSI2 PSI(2)
#define PSI3 PSI(3)
#define PSI4 PSI(4)
#define PSI5 PSI(5)

//static inline double __ipow(double , int );



//---------------- Wendland 2,1 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_2_1_0(double , double );
double __wendlandpsi_2_1_1(double , double );


//---------------- Wendland 3,1 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_3_1_0(double , double );
double __wendlandpsi_3_1_1(double , double );


//---------------- Wendland 3,2 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_3_2_0(double , double );
double __wendlandpsi_3_2_1(double , double );
double __wendlandpsi_3_2_2(double , double );


//---------------- Wendland 4,1 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_4_1_0(double , double );
double __wendlandpsi_4_1_1(double , double );


//---------------- Wendland 4,2 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_4_2_0(double , double );
double __wendlandpsi_4_2_1(double , double );
double __wendlandpsi_4_2_2(double , double );


//---------------- Wendland 4,3 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_4_3_0(double , double );
double __wendlandpsi_4_3_1(double , double );
double __wendlandpsi_4_3_2(double , double );
double __wendlandpsi_4_3_3(double , double );


//---------------- Wendland 5,1 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_5_1_0(double , double );
double __wendlandpsi_5_1_1(double , double );


//---------------- Wendland 5,2 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_5_2_0(double , double );
double __wendlandpsi_5_2_1(double , double );
double __wendlandpsi_5_2_2(double , double );


//---------------- Wendland 5,3 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_5_3_0(double , double );
double __wendlandpsi_5_3_1(double , double );
double __wendlandpsi_5_3_2(double , double );
double __wendlandpsi_5_3_3(double , double );


//---------------- Wendland 5,4 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_5_4_0(double , double );
double __wendlandpsi_5_4_1(double , double );
double __wendlandpsi_5_4_2(double , double );
double __wendlandpsi_5_4_3(double , double );
double __wendlandpsi_5_4_4(double , double );


//---------------- Wendland 6,1 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_6_1_0(double , double );
double __wendlandpsi_6_1_1(double , double );


//---------------- Wendland 6,2 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_6_2_0(double , double );
double __wendlandpsi_6_2_1(double , double );
double __wendlandpsi_6_2_2(double , double );


//---------------- Wendland 6,3 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_6_3_0(double , double );
double __wendlandpsi_6_3_1(double , double );
double __wendlandpsi_6_3_2(double , double );
double __wendlandpsi_6_3_3(double , double );


//---------------- Wendland 6,4 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_6_4_0(double , double );
double __wendlandpsi_6_4_1(double , double );
double __wendlandpsi_6_4_2(double , double );
double __wendlandpsi_6_4_3(double , double );
double __wendlandpsi_6_4_4(double , double );


//---------------- Wendland 6,5 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_6_5_0(double , double );
double __wendlandpsi_6_5_1(double , double );
double __wendlandpsi_6_5_2(double , double );
double __wendlandpsi_6_5_3(double , double );
double __wendlandpsi_6_5_4(double , double );
double __wendlandpsi_6_5_5(double , double );


//---------------- Wendland 7,1 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_7_1_0(double , double );
double __wendlandpsi_7_1_1(double , double );


//---------------- Wendland 7,2 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_7_2_0(double , double );
double __wendlandpsi_7_2_1(double , double );
double __wendlandpsi_7_2_2(double , double );


//---------------- Wendland 7,3 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_7_3_0(double , double );
double __wendlandpsi_7_3_1(double , double );
double __wendlandpsi_7_3_2(double , double );
double __wendlandpsi_7_3_3(double , double );


//---------------- Wendland 7,4 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_7_4_0(double , double );
double __wendlandpsi_7_4_1(double , double );
double __wendlandpsi_7_4_2(double , double );
double __wendlandpsi_7_4_3(double , double );
double __wendlandpsi_7_4_4(double , double );


//---------------- Wendland 7,5 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_7_5_0(double , double );
double __wendlandpsi_7_5_1(double , double );
double __wendlandpsi_7_5_2(double , double );
double __wendlandpsi_7_5_3(double , double );
double __wendlandpsi_7_5_4(double , double );
double __wendlandpsi_7_5_5(double , double );


//---------------- Wendland 7,6 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_7_6_0(double , double );
double __wendlandpsi_7_6_1(double , double );
double __wendlandpsi_7_6_2(double , double );
double __wendlandpsi_7_6_3(double , double );
double __wendlandpsi_7_6_4(double , double );
double __wendlandpsi_7_6_5(double , double );
double __wendlandpsi_7_6_6(double , double );


//---------------- Wendland 8,1 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_8_1_0(double , double );
double __wendlandpsi_8_1_1(double , double );


//---------------- Wendland 8,2 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_8_2_0(double , double );
double __wendlandpsi_8_2_1(double , double );
double __wendlandpsi_8_2_2(double , double );


//---------------- Wendland 8,3 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_8_3_0(double , double );
double __wendlandpsi_8_3_1(double , double );
double __wendlandpsi_8_3_2(double , double );
double __wendlandpsi_8_3_3(double , double );


//---------------- Wendland 8,4 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_8_4_0(double , double );
double __wendlandpsi_8_4_1(double , double );
double __wendlandpsi_8_4_2(double , double );
double __wendlandpsi_8_4_3(double , double );
double __wendlandpsi_8_4_4(double , double );


//---------------- Wendland 8,5 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_8_5_0(double , double );
double __wendlandpsi_8_5_1(double , double );
double __wendlandpsi_8_5_2(double , double );
double __wendlandpsi_8_5_3(double , double );
double __wendlandpsi_8_5_4(double , double );
double __wendlandpsi_8_5_5(double , double );


//---------------- Wendland 8,6 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_8_6_0(double , double );
double __wendlandpsi_8_6_1(double , double );
double __wendlandpsi_8_6_2(double , double );
double __wendlandpsi_8_6_3(double , double );
double __wendlandpsi_8_6_4(double , double );
double __wendlandpsi_8_6_5(double , double );
double __wendlandpsi_8_6_6(double , double );


//---------------- Wendland 8,7 family -------------------
//-------- Input should be premultiplied by c ------------

double __wendlandpsi_8_7_0(double , double );
double __wendlandpsi_8_7_1(double , double );
double __wendlandpsi_8_7_2(double , double );
double __wendlandpsi_8_7_3(double , double );
double __wendlandpsi_8_7_4(double , double );
double __wendlandpsi_8_7_5(double , double );
double __wendlandpsi_8_7_6(double , double );
double __wendlandpsi_8_7_7(double , double );
#ifdef __cplusplus
}
#endif
#endif
