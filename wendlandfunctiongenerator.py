from fractions import Fraction
from math import gcd
from functools import reduce
import argparse
header="""/* *****************************************************************
    This file is automatically generated, it includes definitions or
    declerations for various wendland functions and families used as RBF 
    functions for collocation problems
***************************************************************** */
"""
squarefunctionprototype="""static double __ipow(double , int );"""

squarefunction="""//Function for fast squaring to integer power
//Posted by user Elias Yarrkov on Stackoverflow.
//Exponentiation by squaring

static double __ipow(double base, int exp)
{
    double result = 1.0;
    for (;;)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }

    return result;
}
"""

hfiletop="""// file:wendlandfunctions.h
#ifndef WENDLANDFUNCTIONS_H_
#define WENDLANDFUNCTIONS_H_
#ifdef __cplusplus
extern "C" {
#endif

#define _join2(x, y,z) __wendlandpsi_##x##_##y##_##z
#define _join(x,y,z) _join2(x,y,z)
#define PSI(N) _join(WL,WK, N)

#define PSI0 PSI(0)
#define PSI1 PSI(1) 
#define PSI2 PSI(2)
#define PSI3 PSI(3)
#define PSI4 PSI(4)
#define PSI5 PSI(5)
"""

hfilebot="""#endif"""

cfiletop="""// file:wendlandfunctions.c
#include "wendlandfunctions.h"
"""

cfilebot=""""""
def wendlanditeration(p,k):
    der=polyderivative(p)
    derfact=polymul(p,[-k])
    der=polymul(der,[1,-1])
    derfact=polyadd(derfact,der)
    derfact,junk=polydivision(derfact,[0,1])
    derfact,factor=factornumerators(derfact)
    return derfact,factor

def polyderivative(x):
    res=[Fraction(0,1)]*polydeg(x)
    if(polydeg(x)==0):
        return [Fraction(0,1)]
    for i in range(polydeg(x)):
        res[i]=(i+1)*x[i+1]
    return res

def wendland(l,k):
    assert(k>0)
#    print("""Wendland""")
    pp=integrand(l,k)
#    print(strmulpoly(pp))
    I=integratemulpoly(pp)
#    print(strmulpoly(I))
    up=evalmulpoly(I,1)
#    print(strpoly(up))
    low=monoevalmulpoly(I,1)
    low=polymul(low,[-1])
    res=polyadd(up,low)
#    print("""Wendland done""")
    return res

def polydivision(x,y):
    if(polydeg(x)<polydeg(y)):
        return [Fraction(0,1)], x
    remainder=x
    factor=[Fraction(0,1)]
    while(polydeg(remainder)>=polydeg(y)):
        p=polydeg(remainder)-polydeg(y)
        temp=[Fraction(0,1)]*p
        temp.append(remainder[polydeg(remainder)]/y[polydeg(y)])
        temp2=polymul(y,temp)
        remainder=polysub(remainder,temp2)
        factor=polyadd(factor,temp)
    return factor,remainder

def factornumerators(x):
    f=lambda x:x.numerator
    nume=list(map(f,x))
    g=lambda a,b: int(gcd(a,b))
    smallest=abs(reduce(g,nume[1:],nume[0]))
    if(x[polydeg(x)]<=0):
        smallest=-smallest
    #print(x)
    res=polymul(x,[Fraction(1,smallest)])
    return res,smallest

def cleardenoms(x):
    f=lambda x:x.denominator
    denoms=list(map(f,x))
    lcm=lambda a,b: int(a*b/gcd(a,b))
    smallest=reduce(lcm,denoms[1:],denoms[0])
    res=polymul(x,[smallest])
    return res,smallest

def reducepoly(x):
    lastindex=[i for i,e in enumerate(x) if e!=0]
    if(len(lastindex)>0):
        return x[0:lastindex[-1]+1]
    return [Fraction(0,1)]

def polydeg(x):
    return len(reducepoly(x))-1

def polysub(a,b):
    res=polyadd(a,polymul(b,[-1]))
    res=reducepoly(res)
    return res

def integrand(l,k):
    p=[1,-1]
    mid=[0,1]
    tt=[[0,0,-1],[0],[1]]  #(t^2-r^2)
    
    p=list(map(Fraction,p))
    mid=list(map(Fraction,mid))

    rr=[1]
    for i in range(l):
        rr=polymul(rr,p)
    rr=polymul(rr,mid)
    
    acc=[[1]]
    for i in range(k-1):
        acc=mulpolymul(acc,tt)
    
    rr=poly2mulpoly(rr)
    res=mulpolymul(acc,rr)
    return res

def strpoly(x,chr='x'):
    msg=""
    p=0
    sgn=True
    for i in x:
#        if(i==0):
#            p=p+1
#            continue
        if(p==0):
            msg=str(i)+msg
        else:
            t=str(i)+chr+"^"+str(p)
            if(sgn):
                msg=t+" + " +msg
            else:
                msg=t+" " +msg
        sgn=(i>=0)
        p=p+1
    return msg   

def strpolyhorner(x,chr='x'):
    msg=""
    p=0
    sgn=True
    for i in x:
        if(p==0):
            msg=str(i)+msg
        else:
            t=str(i)
            if(sgn):
                msg=t+")*"+chr+" + " +msg
            else:
                msg=t+" " +msg
        sgn=(i>=0)
        p=p+1
    for i in range(p-1):
        msg="("+msg
    return msg 

def integratemulpoly(y):
    res=[[0]]*(len(y)+1)
    for i in range(0,len(y)):
        f=lambda x : x/(i+1)
        res[i+1]=list(map(f,y[i]))
    return res

def polymulmono(x,power):
    t=[0]*power
    t.append(1)
    res=polymul(x,t)
    return res

def monoevalmulpoly(r,power):
    res=[0]
    for i in range(len(r)):
        res=polyadd(polymulmono(r[i],power*i),res)
    return res

def evalmulpoly(x,point):
    res=[0]
    for i in range(len(x)):
        f=lambda x:(point**i)*x
        t=list(map(f,x[i]))
        res=polyadd(res,t)
    return res

def poly2mulpoly(x):
    g=lambda x : [x]
    return list(map(g,x))

def polyadd(x,y):
    big=y
    small=x
    if len(x)>=len(y):
        big=x
        small=y

    res=[0]*len(big)
    for i in range(len(big)):
        if(i<len(small)):
            res[i]=big[i]+small[i]
        else:
            res[i]=big[i]
    return reducepoly(res)

def mulpolymul(x,y):
    res=[[0]]*(len(x)+len(y)-1)
    for i in range(len(x)):
        for j in range(len(y)):
            res[i+j]=polyadd(polymul(x[i],y[j]),res[i+j])
    return res

def mulpolyadd(x,y):
    big=y
    small=x
    if len(x)>=len(y):
        big=x
        small=y
    res=[[0]]*len(big)
    for i in range(len(big)):
        if(i<len(small)):
            res[i]=polyadd(big[i],small[i])
        else:
            res[i]=big[i]
    return res


def strmulpoly(x,chr1='x',chr2='r'):
    msg=""
    p=0
    for i in x:
        if(p==0):
            msg="("+strpoly(i,chr2)+")"+msg
        else:
            msg="("+strpoly(i,chr2)+")"+chr1+"^"+str(p)+" + "+msg
        p=p+1
    return msg

def polymul(x,y):
    res=[0]*(len(x)+len(y)-1)
    for i in range(len(x)):
        for j in range(len(y)):
            res[i+j]=res[i+j]+x[i]*y[j]
    return res

def polyfactor(x):
    poly=x
    factor,remainder=polydivision(poly,[1,-1])
    t=[Fraction(1,1)]
    p=0
    while(remainder==[0]):
        poly=factor
        p=p+1
        t=polymul(t,[1,-1])
        factor,remainder=polydivision(poly,[1,-1])
    return poly,p

def functionheadertemplate(l,k,i):
    signature="""double __wendlandpsi_"""+str(l)+"_"+str(k)+"_"+str(i)+"""(double , double );"""
    return signature

def functiontemplate(l,k,i,p,poly,factor):
    signature="""double __wendlandpsi_"""+str(l)+"_"+str(k)+"_"+str(i)+"""(double x, double c){
    double t=__ipow((1.0-x),"""+str(p)+""");
"""
    signature=signature+"""    t="""+str(factor)+"*t*"
    for j in range(i):
        signature=signature+"(c*c)*"
    signature=signature+"""("""+strpolyhorner(poly)+""");
    return t;
}"""
    return signature

def main(l,k,pretty):
    if(pretty):
        pfun = strpoly
    else:
        pfun = strpolyhorner
    b=wendland(l,k)
    ib,denom=cleardenoms(b)
    poly,p=polyfactor(ib)
    derfact,factor=wendlanditeration(poly,p)
    if(pretty): 
        print("original:", strpoly(b), " with factor ", denom)
        print("psi_0 Wendland(",l,",",k,") is: == (1-x)^",p,"*(",pfun(poly),")")
        print("psi_1 Wendland(",l,",",k-1,") is: == ",factor,"*(1-x)^",(p-1),"*(",pfun(derfact),")") 
    else:
        #generate the code output
        ccode=""
        hcode=""
        for L in range(2,l+1):
            for K in range(1,L):
                b=wendland(L,K)
                poly,denom=cleardenoms(b)
                poly,p=polyfactor(poly)
                ff=1.0
                c2k=polydeg(poly)
                ccode=ccode+"\n\n//---------------- Wendland "+str(L)+"," +str(K)+" family -------------------\n"
                ccode=ccode+"//-------- Input should be premultiplied by c ------------\n\n"
                hcode=hcode+"\n\n//---------------- Wendland "+str(L)+"," +str(K)+" family -------------------\n"
                hcode=hcode+"//-------- Input should be premultiplied by c ------------\n\n"
                for i in range(c2k+1):
                    ccode=ccode+functiontemplate(L,K,i,p-i,poly,ff)+"\n"
                    hcode=hcode+functionheadertemplate(L,K,i)+"\n"
                    if(i<c2k):
                        poly,factor=wendlanditeration(poly,p-i)
                        ff=ff*factor
        cfile = header + cfiletop + "\n\n"+ squarefunction+"\n\n"+ccode + cfilebot
        hfile = header + hfiletop+ "\n\n"+squarefunctionprototype +"\n\n"+hcode+hfilebot
        print(cfile)
        print("\n\n<<<<<<<<<<<<<<<<<<<  CUT HERE  >>>>>>>>>>>>>>>>>>>\n\n")
        print(hfile)

#    print("-------------")
#    test()

def test():
        x=[1,-2,3]
        y=[x,x,x]
        print(strpoly(x))
        print(strmulpoly(y))
        print(strpoly(polymul(x,x)))
        print(strpoly(polyadd(x,x)))
        print(strmulpoly(mulpolymul(y,y)))
        z=evalmulpoly(y,1)
        k=mulpolymul(y,y)
        print(strpoly(z))
        print(strpoly(evalmulpoly(k,1)))
        print(strpoly(polymulmono(x,1)))
        print(strpoly(polymulmono(x,2)))
        print(strpoly(polymulmono(x,5)))
        print(strpoly(monoevalmulpoly(y,1)))
        print(strmulpoly(integratemulpoly(y)))
        print(strmulpoly(integrand(3,1)))
        b=wendland(3,1)
        ib,denom=cleardenoms(b)
        print(strpoly(b))
        print(strpoly(ib))
        print(strpoly(polysub(ib,ib)))
        a=[1,2,1]
        c=[-1,1]
        f,r=polydivision(a,c)
        print("Division gives:",strpoly(a), "/", strpoly(c), " is ", strpoly(f), " remainder ", strpoly(r))


if __name__=='__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument('--l',type=int, default=5)
    parser.add_argument('--k',type=int, default=3)
    parser.add_argument('--p',type=int, default=0)
    a=parser.parse_args()
    l,k,p=a.l,a.k,a.p
    main(l,k,p)
