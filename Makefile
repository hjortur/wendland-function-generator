CC=gcc
LFLAGS=-lm -lcblas -llapacke
CFLAGS=-Winline -O2
HEADS=point.h wendlandfunctions.h  
SOURCE=point.c wendlandfunctions.c 
HEADSDDE=ddecolloc.h
SOURCEDDE=ddecolloc.c
HEADSSDE=sdecolloc.h
SOURCESDE=sdecolloc.c
RESDIR=results

all: zubov 1dim siggi vanderpol
.PHONY : all

zubov: zubov.c $(HEADS) $(HEADSSDE) $(SOURCE) $(SOURCESDE)
	$(CC) -DDIM=2 -DWL=8 -DWK=6 -Drbfc=1.0 -o $@ $^ $(CFLAGS) $(LFLAGS)

1dim: 1dim.c $(HEADS) $(HEADSSDE) $(SOURCE) $(SOURCESDE)
	$(CC) -DDIM=1 -DWL=7 -DWK=6 -Drbfc=2.0 -o $@ $^ $(CFLAGS) $(LFLAGS)

siggi: siggi.c $(HEADS) $(HEADSDDE) $(SOURCE) $(SOURCEDDE)
	$(CC) -DDIM=2 -DWL=5 -DWK=3 -Drbfc=1.0 -o $@ $^ $(CFLAGS) $(LFLAGS)

vanderpol: vanderpol.c $(HEADS) $(HEADSDDE) $(SOURCE) $(SOURCEDDE)
	$(CC) -DDIM=2 -DWL=6 -DWK=4 -Drbfc=1.0 -o $@ $^ $(CFLAGS) $(LFLAGS)

.PHONY: computation
computation: $(RESDIR)/zubov.dat $(RESDIR)/1dim.dat $(RESDIR)/siggi.dat $(RESDIR)/vanderpol.dat


$(RESDIR)/%.dat: %
	@mkdir -p $(RESDIR)
	./$^ > ./$@

RESDEP=$(RESDIR)/zubov.dat $(RESDIR)/1dim.dat $(RESDIR)/siggi.dat $(RESDIR)/vanderpol.dat
FIGS=figures

.PHONY: graphs
graphs:$(FIGS)/1dim.eps $(FIGS)/zubov.eps $(FIGS)/vanderpol.eps $(FIGS)/siggi.eps

$(FIGS)/1dim.eps: $(RESDIR)/1dim.dat
	@mkdir -p $(FIGS)
	gnuplot -e "set terminal postscript eps; set output '$@'; set contour; set title 'Lyapunov function V(x)'; plot '$^' with lines notitle" 

$(FIGS)/%.eps: $(RESDIR)/%.dat
	@mkdir -p $(FIGS)
	gnuplot -e "set terminal postscript eps; set output '$@'; set dgrid3d 20,20; set contour; set title 'Surface and contour plot of Lyapunov function'; splot '$^' with lines notitle"


