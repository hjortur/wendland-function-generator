#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <cblas.h>
#include <lapacke.h>
#include "wendlandfunctions.h"
#include "point.h"
#include "sdecolloc.h"

#define NX 20
#define NY 80
#define NI 4
#define NO 40

#define SMALL_R 0.4
#define BIG_R 1.9
#define RANGE_X 2.0




struct point f(struct point x)
{
    struct point ret,tmp,tmp2;
    tmp.x[0]=normp(x)-1.0;
    tmp.x[1]=1.0;
    tmp2.x[0]=-1.0;
    tmp2.x[1]=normp(x)-1.0;

    ret.x[0]=innerp(tmp,x);
    ret.x[1]=innerp(tmp2,x);
    
    return ret;
}


int dimg(void)
{
	return 1;
}

struct point *g(struct point X,int *size)
{
	int Q=dimg(); //dimension of brownian motion should be less then dim
	struct point *ret=malloc(Q*sizeof(*ret));
	double tmp = normp(X)*(normp(X)-0.5)*(normp(X)-1.5);
	ret->x[0] = tmp*X.x[0];
	ret->x[1] = tmp*X.x[1];
	*size=Q;
	return ret;
}


double rhsv(struct point x)
{
	double ret=(normp(x)-SMALL_R)/(BIG_R-SMALL_R);
	return ret;
}

double rhslv(struct point x)
{
    return -0.01;
}


int main(int argc, char **argv)
{
    int sizeY=0,sizeS=NI+NO, info;
    struct point a={-2.0,-2.0};
    struct point b={2.0,2.0};
    struct point Ny={NY,NY};

    //struct point *Y=generategrid(a,b,Ny,SMALL_R,10.0,&sizeY);
    struct point *Y=generateoptimalgrid(a,b,4.0/100.0, SMALL_R, 10.0, &sizeY); 
    struct point *S=generate2dboundarygrid(SMALL_R, BIG_R,NI,NO);
    double *A=collocmatrixpacked(Y,sizeY,S, sizeS);
    double *r=rhs_v(Y,sizeY, S,sizeS);
    info=LAPACKE_dpptrf(LAPACK_ROW_MAJOR,'U', sizeY+sizeS, A);
    //info=LAPACKE_dposv(LAPACK_ROW_MAJOR,'U', sizeY+sizeS, 1,A, sizeY+sizeS, r, 1);
    if( info>0)
    {
        printf("the leading minor of order %d of Collocmatrix is not positive definite.\nSolution could not be computed\n", info);
    }

    info=LAPACKE_dpptrs(LAPACK_ROW_MAJOR, 'U', sizeY+sizeS, 1, A, r, 1);
    if(info !=0)
    {
	    printf("parameter %d had illegal value\n", -info);
    } 
    struct point Nx={NX,NX};
    int sizeX;
    struct point *X=generategrid(a,b, Nx,-1.0,10.0, &sizeX);
    double *xvalues=evalfunc(X,sizeX, Y, sizeY,S,sizeS, r);
    
    printresults(X,sizeX,xvalues);
    
    free(xvalues);
    free(X);
    free(r);
    free(S);
    free(A);
    free(Y);
    return 0;
}
