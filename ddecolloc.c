#include <malloc.h>
#include <cblas.h>
#include <lapacke.h>
#include "ddecolloc.h"
#include "wendlandfunctions.h"
#include "point.h"

#ifndef WL
#error "Must define constant WL"
#endif

#ifndef WK
#error "Must define constant WK"
#endif

#ifndef rbfc
#error "Must define constant rbfc"
#endif



struct point f(struct point x);
double rhslv(struct point x);

double *rhs_v(struct point *x, int size)	
{
    double *gamma=malloc(size*sizeof(*gamma));
    for(int i=0;i<size;i++)
    {
        gamma[i]=rhslv(x[i]);
    }
    return gamma;
}
double Ay(struct point Y, struct point X)
{
	struct point diff=addp(Y,X,1.0,-1.0);
	struct point FX=f(X);
	double temp=normp(diff)*rbfc;
	if(temp>=1.0)
		return 0.0;
	
	double ret=-PSI1(temp,rbfc)*innerp(diff,FX);
	return ret;
}
double Axy(struct point Y, struct point X)
{
	struct point diff=addp(Y,X,1.0,-1.0);
	struct point FX=f(X);
	struct point FY=f(Y);
	double temp=normp(diff)*rbfc;
	if(temp>=1.0)
		return 0.0;
	
	double ret=PSI2(temp, rbfc) * innerp(diff,FY)*innerp(mulp(diff,-1.0), FX) - PSI1(temp,rbfc)*innerp(FY,FX);
	return ret;
}


double *collocmatrix(struct point *Y, int sizeY)
{
    double *A=malloc((sizeY*sizeY)*sizeof(*A));
    for(int j=0;j<sizeY;j++)
    {
        for(int k=0;k<sizeY;k++)
        {
		A[sizeY*j + k] =Axy( Y[j], Y[k]);
        }
    }
    return A;
}

double *evalfunc(struct point *X, int sizeX, struct point *Y, int sizeY, double *alpha)
{
    double *res=malloc(sizeX*sizeof(*res));
    double *M = malloc(sizeX*sizeY*sizeof(*M));
    double temp;
    struct point diff;
    for(int j=0;j<sizeX;j++)
    {
        for(int k=0;k<sizeY;k++)
        {
                M[sizeY*j+k]=Ay(X[j],Y[k]);
        }
    }
    cblas_dgemv(CblasRowMajor, CblasNoTrans, sizeX,sizeY,1, M, sizeY, alpha, 1, 0.0,res,1);
    free(M);
    return res;
}
