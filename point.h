#ifndef POINT_H
#define POINT_H

#ifndef DIM
#error "DIM must be defined"
#endif

struct point{
    double x[DIM];
};

struct _grid{
	struct point *grid;
	int size;
	int current;
};

struct point constpoint(double);

double delta(int,int);
double proj(struct point, int);

struct point *generate2dboundarygrid(double r, double R, int sizeinner, int sizeouter);
struct point *generategrid(struct point , struct point , struct point , double , double , int *);

struct point *generateoptimalgrid(struct point , struct point , double , double, double, int *);
double *symmetrize(double *, int);

double *outer(struct point x, struct point y);
double *eye(int );
double *matrixadd(double *A, double *B, int m, int n, double alpha, double beta);
double *matrixmul(double *A, int rowa, int cola, double *B, int rowb, int colb);
double trace(double *M, int n);


double innerp(struct point x, struct point y);

double normp(struct point x);

struct point mulp(struct point x, double a);

struct point addp(struct point x, struct point y, double a, double b);

void printmatrix(double *A, int m, int n, const char *s);

void printpoints(struct point *X, int sizeX, const char *s);

void printresults(struct point *X, int sizeX, double *alpha);
double matrixnorm(double *M, int rows, int cols);
double matrixnormpacked(double *M, int n);
#endif
