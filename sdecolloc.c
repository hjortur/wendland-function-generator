#include <malloc.h>
#include <cblas.h>
#include <assert.h>
#include <lapacke.h>
#include "sdecolloc.h"
#include "point.h"
#include "wendlandfunctions.h"

#ifndef WL
#error "Must define constant WL"
#endif

#ifndef WK
#error "Must define constant WK"
#endif

#ifndef rbfc
#error "Must define constant rbfc"
#endif

double rhslv(struct point);
double rhsv(struct point);
struct point f(struct point);
struct point *g(struct point, int *);

double *m_g(struct point x)
{
	int Q=0;
	struct point *gx=g(x,&Q);
    double *M=malloc((DIM*DIM)*sizeof(*M));
	assert(Q<=DIM);
	assert(Q>0);
	double ret=0.0;
    for(int i=0;i<DIM;i++)
    {
        for(int j=0;j<DIM;j++)
        {
            ret=0.0;
	        for(int q=0;q<Q;q++)
	        {
		        ret+=gx[q].x[i]*gx[q].x[j];
	        }
            M[i*DIM+j]=ret;
        }
    }
	free(gx);
	return M;
}

double *rhs_v(struct point *x, int sizeX,struct point *s, int sizeS)
{
    double *gamma=malloc((sizeX+sizeS)*sizeof(*gamma));
    for(int i=0;i<sizeX;i++)
    {
        gamma[i]=rhslv(x[i]);
    }
    for(int i=sizeX;i<sizeX+sizeS;i++)
    {
	gamma[i]=rhsv(s[i-sizeX]);
    }
    return gamma;
}

double Azero_v(struct point XL, struct point XK)
{
    struct point z=addp(XL,XK,1.0,-1.0);
    double temp=normp(z)*rbfc;
    if(temp>=1.0)
        return 0.0;
    return PSI0(temp,rbfc);
}

double Ay_v(struct point XL, struct point XK)
{
    struct point z=addp(XL,XK,1.0,-1.0);
    double norm_z =normp(z);
    double temp=norm_z*rbfc;
    if(temp>=1.0)
        return 0.0;
    struct point fl=f(XL);
    double *ml=m_g(XL);
    double *e=eye(DIM);
    double *ztz = outer(z,z);
    double *ms=matrixadd(ztz,e,DIM,DIM,PSI2(temp,rbfc),PSI1(temp,rbfc));
    double *mlms=matrixmul(ml,DIM,DIM,ms,DIM,DIM);
    double ret=PSI1(temp,rbfc)*innerp(z,fl) + 0.5*trace(mlms,DIM);
    free(ml);
    free(e);
    free(ztz);
    free(ms);
    free(mlms);
    return ret;
}



double Axy_v(struct point XL, struct point XK)
{
    struct point z=addp(XL,XK,1.0,-1.0);
    double norm_z=normp(z);
    double temp=norm_z*rbfc;
    if(temp >=1.0)
        return 0.0;
    
    struct point fl=f(XL),fk=f(XK);
    double *ml = m_g(XL),*mk=m_g(XK);
    double trml=trace(ml,DIM),trmk=trace(mk,DIM);
    double b0,b1,b2,b3,b4,b5,b6,b7,b8,b9;
    
    b0=-PSI1(temp,rbfc)*innerp(fl,fk);
    b1=-PSI2(temp,rbfc)*innerp(z,fl)*innerp(z,fk);
    b2=0.25*PSI2(temp,rbfc)*trml*trmk;
    
    double *mlmk=matrixmul(ml,DIM,DIM,mk,DIM,DIM);
    double *mkml=matrixmul(mk,DIM,DIM,ml,DIM,DIM);
    double *ms=matrixadd(mlmk,mkml,DIM,DIM,1.0,1.0);
    
    free(mlmk);
    free(mkml);
    
    b3=0.25*PSI2(temp,rbfc)*trace(ms,DIM);
    
    struct point *mkz=(struct point *)matrixmul(mk,DIM,DIM,&(z.x[0]), DIM,1);
    struct point *mlz=(struct point *)matrixmul(ml,DIM,DIM,&(z.x[0]),DIM,1);
    
    b4=0.25*PSI3(temp,rbfc)*(trml*innerp(z,*mkz) + trmk*innerp(z,*mlz));
    
    struct point *msz=(struct point *)matrixmul(ms,DIM,DIM,&(z.x[0]),DIM,1);
    
    b5=0.5*PSI3(temp,rbfc)*innerp(*msz,z);
    b6=0.25*PSI4(temp,rbfc)*innerp(z,*mlz)*innerp(z,*mkz);

    struct point *mkfl=(struct point *)matrixmul(mk,DIM,DIM,&(fl.x[0]),DIM,1);
    struct point *mlfk=(struct point *)matrixmul(ml,DIM,DIM,&(fk.x[0]),DIM,1);
    struct point tt=addp(*mkfl, *mlfk, 1.0,-1.0);
    
    b7=PSI2(temp,rbfc)*innerp(z,tt);
    b8=0.5*PSI2(temp,rbfc)*innerp(z,addp(fl,fk,trmk,-trml));
    b9=0.5*PSI3(temp,rbfc)*(innerp(z,fl)*innerp(z,*mkz)-innerp(z,fk)*innerp(z,*mlz));
    
    free(mkz);
    free(mlz);
    free(msz);
    free(mkfl);
    free(mlfk);
    free(ml);
    free(mk);
    free(ms);

    double ret = b0+b1+b2+b3+b4+b5+b6+b7+b8+b9;
    
    return ret;
}

double *collocmatrix(struct point *Y, int sizeY, struct point *S,int sizeS)
{
    double *A=malloc((sizeY+sizeS)*(sizeY+sizeS)*sizeof(*A));
    for(int j=0;j<sizeY;j++)
    {
        for(int k=0;k<sizeY;k++)
        {
		    A[(sizeY+sizeS)*j + k] = Axy_v(Y[j], Y[k]); 
        }
	    for(int k=sizeY; k<sizeY+sizeS; k++)
	    {
		    A[(sizeY+sizeS)*j +k]=Ay_v(Y[j],S[k-sizeY]);
    	}
    }
    for(int j=sizeY;j<sizeY+sizeS;j++)
    {
	    for(int k=0;k<sizeY;k++)
	    {
		    A[(sizeY+sizeS)*j +k]=Ay_v(Y[k],S[j-sizeY]);
	    }
	    for(int k=sizeY;k<sizeY+sizeS;k++)
	    {
		    A[(sizeY+sizeS)*j+k]=Azero_v(S[j-sizeY],S[k-sizeY]);
	    }
    }

    return A;
}

double *collocmatrixpacked(struct point *Y, int sizeY, struct point *S, int sizeS)
{
	size_t n = sizeY+sizeS;
	double *A=malloc((n*(n+1))/2 * sizeof(*A));
	int t=0;
	for(int j=0;j<sizeY;j++)
	{
		for(int k=j;k<sizeY;k++)
		{
			A[t+(k-j)]=Axy_v(Y[j],Y[k]);
		}
		for(int k=sizeY;k<sizeY+sizeS;k++)
		{
			A[t+(k-j)]=Ay_v(Y[j],S[k-sizeY]);
		}
		t=t+(n-j);
	}
	for(int j=sizeY;j<sizeY+sizeS;j++)
	{
		for(int k=j;k<sizeY+sizeS;k++)
		{
			A[t+(k-j)]=Azero_v(S[j-sizeY],S[k-sizeY]);
		}
		t=t+(n-j);
	}
	return A;
}

double *evalfunclv(struct point *X, int sizeX, struct point *Y, int sizeY, struct point *S,int sizeS,double *alpha)
{
    double *res=malloc(sizeX*sizeof(*res));
    double *M = malloc(sizeX*(sizeY+sizeS)*sizeof(*M));
    double temp;
    struct point diff;
    for(int j=0;j<sizeX;j++)
    {
        for(int k=0;k<sizeY;k++)
        {
            diff=addp(Y[k],X[j],1.0,-1.0);
            temp=normp(diff)*rbfc;
            if(temp>=1.0)
                M[(sizeY+sizeS)*j+k]=0.0;
            else
            {
                M[(sizeY+sizeS)*j+k]=Axy_v(Y[k], X[j]);
            }
        }
	for(int k=sizeY;k<sizeY+sizeS;k++)
	{
		diff=addp(S[k-sizeY], X[j], 1.0,-1.0);
		temp=normp(diff)*rbfc;
		if(temp>=1.0)
			M[(sizeY+sizeS)*j +k]=0.0;
		else
		{
			M[(sizeY+sizeS)*j+k]=Ay_v(S[k-sizeY],X[j]);
		}
	}
    }
    //printf("Got here \n");
    //printmatrix(&M[(sizeY+sizeS)*(99-28)], 28,sizeY+sizeS, "M is");
    cblas_dgemv(CblasRowMajor, CblasNoTrans, sizeX,sizeY+sizeS,1, M, sizeY+sizeS, alpha, 1, 0.0,res,1);
    //printf("Finished multiplying\n");
    free(M);
    return res;
}

double *evalfunc(struct point *X, int sizeX, struct point *Y, int sizeY, struct point *S,int sizeS,double *alpha)
{
    double *res=malloc(sizeX*sizeof(*res));
    double *M = malloc(sizeX*(sizeY+sizeS)*sizeof(*M));
    double temp;
    struct point diff;
    for(int j=0;j<sizeX;j++)
    {
        for(int k=0;k<sizeY;k++)
        {
            /*diff=addp(Y[k],X[j],1.0,-1.0);
            temp=normp(diff)*rbfc;
            if(temp>=1.0)
                M[(sizeY+sizeS)*j+k]=0.0;
            else
            {
                M[(sizeY+sizeS)*j+k]=Ay(temp,diff,Y[k], X[j]);
            }*/
            M[(sizeY+sizeS)*j+k]=Ay_v(Y[k],X[j]);
        }
	for(int k=sizeY;k<sizeY+sizeS;k++)
	{
		/*diff=addp(S[k-sizeY], X[j], 1.0,-1.0);
		temp=normp(diff)*rbfc;
		if(temp>=1.0)
			M[(sizeY+sizeS)*j +k]=0.0;
		else
		{
			M[(sizeY+sizeS)*j+k]=Azero(temp,diff,S[k-sizeY],X[j]);
		}*/
        M[(sizeY+sizeS)*j+k]=Azero_v(X[j],S[k-sizeY]);
	}
    }
    //printf("Got here \n");
    //printmatrix(M, sizeX,sizeY+sizeS, "M is");
    cblas_dgemv(CblasRowMajor, CblasNoTrans, sizeX,sizeY+sizeS,1, M, sizeY+sizeS, alpha, 1, 0.0,res,1);
    //printf("Finished multiplying\n");
    free(M);
    return res;
}
