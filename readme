example run
python wendlandfunctiongenerator.py > test

parameters:
--p 0   (default) outputs c and header file for wendlandfunctions from W_2,1 up to W_l_(l-1)
--p 1   outputs wendland functions for specified values of l and k in human readable format

--l n   (default 5) selects the value of l to use
--k n   (default 3) selects the value of k to use (unused for --p 0)

output is a string to stdout, when --p 0, the part above <<< CUT HERE >>> is the .c file 
and the part below is the .h file.
When --p 0 the script will output all the wendland function families used for collocation problems up to 
and including the number specified with --l n.

Example: when ran with --l 4 it will give the families for 
    W_2_1 -- two functions
    W_3_1 -- two functions
    W_3_2 -- three functions
    W_4_1 -- two functions
    W_4_2 -- three functions
    W_4_3 -- four functions

The files wendlandfunctions.c and wendlandfunctions.h contain the function definition
and declarations of all the Wendland functions up to order 8.

The functions do not contain any error checking, so the user must take care to supply the
correct input. Each function takes two doubles as an argument, the first one is
c*̣||x|| and is only valid for values on the interval [0,1], and the second is c
where 1/c is the radius of support for each function.
